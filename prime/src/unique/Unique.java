package unique;

import java.util.Scanner;

public class Unique {
	public static void main(String arg[]) {
		int arr[] = new int[5];
		Scanner sc = new Scanner(System.in);

		for (int i = 0; i < 5; i++) {
			arr[i] = sc.nextInt();

		}
		System.out.println(getsum(arr));
	}

	public static int getsum(int arr[]) {
		int sum = 0;
		int count = 0;
		for (int i = 0; i < 5; i++) {
			count = 0;
			for (int j = 0; j < 5; j++) {
				if (arr[i] == arr[j])
					count++;

			}
			if (count == 1)
				sum = sum + arr[i];
		}
		return sum;

	}
}