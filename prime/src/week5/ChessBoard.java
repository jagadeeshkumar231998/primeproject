package week5;

import java.util.Scanner;

public class ChessBoard {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int currentPosition[] = new int[2];

		System.out.println("Enter current position");

		for (int i = 0; i < 2; i++) {

		currentPosition[i] = sc.nextInt();
		}

		System.out.println("Total possible moves are " + calcPossibleMoves(currentPosition));
		}

		public static int calcPossibleMoves(int[] currentPosition) {

		int[][] moves = { { 1, -2 }, { -2, -1 }, { 2, 1 }, { 2, -1 }, { -2, 1 }, { -1, 2 }, { -1, -2 }, { 1, 2 } };
		int countPossibleMoves = 0;
		int newXPos, newYPos;

		for (int[] move : moves) {
		newXPos = currentPosition[0] + move[0];
		newYPos = currentPosition[1] + move[1];

		if (newXPos >= 0 && newYPos >= 0 && newXPos < 8 && newYPos < 8) {
		countPossibleMoves++;
		}

		}

		return countPossibleMoves;
		}

		}




