package collatz;

import java.util.Scanner;

public class Collat {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		
		System.out.println(highestPowerOf2InCollatzSeq(num));
		
	}

	public static int highestPowerOf2InCollatzSeq(int num) {
		while (!isPowerOfTwo(num)) {
			num = nextCollatzTerm(num);
		}
		
		return num;
	}

	public static int nextCollatzTerm(int num) {
		if (num % 2 == 1)
			return num * 3 + 1;
		else
			return num / 2;
	}

	public static boolean isPowerOfTwo(int n) {
		if (n == 0)
			return false;
		while (n != 1) {
			if (n % 2 != 0)
				return false;
			n = n / 2;
		}
		return true;
	}
}

/*
 * public static int getcollatz(int num) { if(ispoweroftwo(num)) return num;
 * else { while(num!=1) {
 * 
 * if(num%2!=0) { num=(num*3)+1; if(ispoweroftwo(num)) { return num; } } else {
 * num=num/2; if(ispoweroftwo(num)) {
 * 
 * 
 * return num; } }
 * 
 * } return num; } }
 */
