package twin;

public class TwinPrimes01 {
	public static void main(String[] args) {
        int num1 = 1;
        int num2 = 200;
        System.out.println(twinPrimes(num1, num2));
    }

    public static String twinPrimes(int start, int limit) {
    	String str="";
        if(start<=0&&limit<=0)
        {
        	str=str+"-1";
        	return str;
        }
        else  if(start>=limit)
        {
        	str=str+"-2";
        	return str;
        }
        else
        {
        for(int i=start+1;i<=limit;i++)
        {
        	if(isPrime(i)&&isPrime(i+2))
        		str=str+(i)+":"+(i+2)+",";
        }
        if(str.isEmpty())
        	return "-3";
        	
        return str.substring(0, str.length()-1);
  
        	
        }
       
    }

    public static boolean isPrime(int num) {
       
        for(int j=2;j<num;j++)
        {
        	if(num%j==0)
        		return false;
        }
        return true;
    }

}


