package nextnumber;

import java.util.Scanner;

public class Palindrome {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		System.out.println(getnextpalindrome(num));

	}

	public static int getnextpalindrome(int num) {
		while (!ispalindrome(num)) {
			num++;
		}
		return num;
	}

	public static boolean ispalindrome(int num) {
		return (num == reverse(num));
	}

	public static int reverse(int num) {
		int rev = 0;
		int remainder;
		while (num > 0) {
			remainder = num % 10;
			rev = rev * 10 + remainder;
			num /= 10;
		}
		return rev;
	}

}
