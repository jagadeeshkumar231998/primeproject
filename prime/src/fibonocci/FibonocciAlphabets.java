package fibonocci;

public class FibonocciAlphabets {
	public static void main(String[] args) {
		System.out.println(getWordValue("MAN"));
	}
	
	public static int getWordValue(String word){
		int sum = 0;
		String alphabetics = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int[] fibArray = fibonacci(26);
		
		for(int i = 0; i < word.length(); i++){
			char ch = word.charAt(i);
			int index = alphabetics.indexOf(ch);
			sum += fibArray[index];
		}
		
		return sum;
	}

	public static int[] fibonacci(int terms) {
		int f[] = new int[terms];

		f[0] = 0;
		f[1] = 1;

		for (int i = 2; i < f.length; i++) {
			f[i] = f[i - 1] + f[i - 2];
		}
		
		return f;

	}

	
}
