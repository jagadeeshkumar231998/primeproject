package factors;

import java.util.Scanner;

public class Factors {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		System.out.println(getsumoffactors(num));
	}

	public static int getsumoffactors(int num) {
		int sum = 0;
		for (int i = 1; i < num; i++) {
			if (num % i == 0)
				sum = sum + i;
		}
		return sum;
	}

}
