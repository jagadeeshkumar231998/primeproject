package factors;

public class MatchingParanthesis {
	

		public static void main(String[] args) {

		// TODO Auto-generated method stub
		System.out.println(isMatchingParenthesis("(2+3)"));
		System.out.println(isMatchingParenthesis("((a + b) * c)"));
		System.out.println(isMatchingParenthesis("(a + b) * c)"));
		System.out.println(isMatchingParenthesis(")(a + b) * c))"));

		}

		public static boolean isMatchingParenthesis(String text) {

		int count=0;
		if(text==null)
		return false;
		if(text.isEmpty())
		return true;
		for(int i=0;i<text.length();i++)
		{
		char c=text.charAt(i);
		if(c=='('||c==')')
		count++;
		}
		if(count%2!=0)
		return false;
		return true;

		}

		}


