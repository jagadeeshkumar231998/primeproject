package week6;

import java.util.*;

public class Crossword {
	public static void main(String arg[]) {

		char[][] arr = initializeCrossword();

		int[][] arr1 = numberBeingCrossword(arr);
		display(arr, arr1);
		System.out.println(getCrossWords(arr, arr1));
		System.out.println(getDownWords(arr,arr1));
	}
	public static char[][] initializeCrossword() {
		char[][] arr = new char[8][9];
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {

				arr[i][j] = sc.next().charAt(0);
			}
		}
		return arr;
	}

	public static int[][] numberBeingCrossword(char[][] arr) {
		int[][] arr1 = new int[8][9];
		int num = 1;
		for (int i = 0; i < 8; i++)

		{

			for (int j = 0; j < 9; j++)

			{
				if (arr[i][j] != '*') {

					if (arr[i - 1][j] == '*' || arr[i][j - 1] == '*') {
						arr1[i][j] = num;
						num++;
					}
				}
			}
		}
		return arr1;
	}

	public static void display(char[][] arr, int[][] arr1) {
		for (int i = 0; i < 8; i++)

		{

			for (int j = 0; j < 9; j++)

			{
				System.out.print(arr[i][j]);
			}
			System.out.println();

		}
		for (int i = 0; i < 8; i++)

		{

			for (int j = 0; j < 9; j++)

			{
				System.out.print(arr1[i][j]);
			}
			System.out.println();
		}
	}

	public static List<String> getCrossWords(char[][] arr, int[][] arr1) {
		String s;
		List<String> wordList = new ArrayList<String>();
		int num;
		for (int i = 0; i < arr.length; i++) {
			s = "";
			for (int j = 0; j < arr[i].length; j++) {

				if (arr[i][j] != '*') {
					s = s + arr[i][j];

				} else {
					if (s != "") {
						num = arr1[i][j - s.length()];
						wordList.add(num + "." + s);
						s = "";
					}
				}

			}

		}
		return wordList;

	}
	public static Map<Integer,String> getDownWords(char[][] arr, int[][] arr1) {
		String s;
		Map<Integer,String> wordMap= new TreeMap<Integer,String>();
		int num;
		for (int j = 0; j < arr.length; j++) {
			s = "";
			for (int i = 0; i < arr.length; i++) {

				if (arr[i][j]!= '*') {
					s = s + arr[i][j];

				} else {
					if (s != "") {
						num = arr1[i - s.length()][j];
						wordMap.put(num, s);
						s = "";
					}
				}

			}

		
		
	}
		
		return wordMap;
}
  
}
