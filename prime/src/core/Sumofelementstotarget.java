package core;

import java.util.*;

public class Sumofelementstotarget {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int k = sc.nextInt();
		int arr[] = initializeArray();

		System.out.println(isReachedTarget(arr, k));
	}

	public static int[] initializeArray() {
		Scanner Sc = new Scanner(System.in);
		int sourceArray[] = new int[6];
		for (int i = 0; i < 6; i++)

		{
			sourceArray[i] = Sc.nextInt();
		}
		return sourceArray;
	}

	public static boolean isReachedTarget(int arr[], int target) {
		HashSet<Integer> s = new HashSet<Integer>();

		for (int i = 0; i < arr.length; ++i) {
			int temp = target - arr[i];

			if (s.contains(temp)) {
				return true;
			}
			s.add(arr[i]);
		}
		return false;
	}
}
