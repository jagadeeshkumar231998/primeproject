package core;

import java.util.*;

public class Distinct {
	public static void main(String[] args) {
		int arr[] = { 1, 2, 2, 1, 3, 4, 3, 5 };
		distinctSum(arr);

	}

	public static void distinctSum(int arr[]) {
		int sum = 0;
		Set<Integer> s = new HashSet<Integer>();
		for (int i : arr) {
			s.add(i);
		}
		for (int i : s) {
			sum = sum + i;
		}
		System.out.println(sum);
	}
}