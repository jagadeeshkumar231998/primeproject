package diagonalelements;

import java.util.Scanner;

public class Diagonal {
	public static void main(String[] args) {
		
	
	int [][]arr=new int[3][3];
	getInputValues(arr);
	getSumOfDiagonaElements(arr);
	}
	public static void getInputValues(int[][] arr)
	{
		Scanner s=new Scanner(System.in);
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				arr[i][j]=s.nextInt();
			}
		}
	}
	public static void getSumOfDiagonaElements(int[][]arr)
	{
		int sum=0;
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				if(i==j||(i-j)==2||(j-i)==2)
					sum=sum+arr[i][j];
			}
		}
		System.out.println(sum);
	}
	

} 
